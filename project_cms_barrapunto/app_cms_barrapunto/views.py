from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from .models import Page
from xml.sax.handler import ContentHandler # Reconocedores de sax
from xml.sax import make_parser
import sys
import string
import urllib.request

form = """
<form action="" method="POST">
    Formulario:<br><br>
    <input type="type" name="rec" value='{value}'><br><br>
    <input type="submit" value="send"><br>
"""

body = ""
global url_barrapunto

def parse(url_barrapunto):
    html_template = """<!DOCTYPE html>
    <html lang="en">
      <body>
        {body}
      </body>
    </html>
    """

    html_item_template = "<li><a href='{link}'>{title}</a></li>"

    class CounterHandler(ContentHandler):
        def __init__ (self):
            self.inContent = 0
            self.theContent = ""
            self.en_noticia = False
            self.title = ""
            self.link = ""

        def startElement (self, name, attrs):
            if name == 'item': # Si la etiqueta empieza por item
                self.en_noticia = True # significa que estamos en una noticia

            elif name == 'title': # Si la etiqueta empieza por title
                if self.en_noticia: # y estamos en una noticia
                    self.inContent = 1 # entonces significa que hay contenido que nos interesa

            elif name == 'link': # Si la etiqueta empieza por link
                if self.en_noticia: # y estamos en una noticia
                    self.inContent = 1 # significa que hay contenido

        def endElement(self, name):
            global body
            if self.en_noticia == True:
                if name == 'title':
                    title = self.theContent
                    self.title = title

                elif name == 'link':
                    link = "<li> Link: <a href=" + self.theContent + ">" + self.theContent + "</a></li>"
                    self.link = link

                elif name == 'item':
                    self.en_noticia = False
                    body += html_item_template.format(link=self.link, title=self.title)

                if self.inContent:
                    self.inContent = 0
                    self.theContent = ""

        def characters(self, chars):
            if self.inContent:
                self.theContent = self.theContent + chars


    NoticiaParser = make_parser() # Parse sax.
    NoticiaHandler = CounterHandler() # Reconozco las cosas que me interesan.
    NoticiaParser.setContentHandler(NoticiaHandler) # Le digo al parser generico que cosas me interesan

    xmlFile = urllib.request.urlopen(url_barrapunto)
    NoticiaParser.parse(xmlFile)
    return (html_template.format(body=body))

url_barrapunto = parse("http://barrapunto.com/index.rss")

def index(request):
    if request.method == "GET":
        pages = Page.objects.all()
        if len(pages) == 0:
            response = "No pages yet."
        else:
            response=""
            for p in pages:
                response += "Nombre: " + p.name + ". Contenido: " + p.content + ".<br>"
    return HttpResponse("<h3> Paginas añadidas: </h3>" + response + "<h3> Titulares de barrapunto: </h3>" + url_barrapunto)

@csrf_exempt
def page(request, resource):
    if request.method == "GET":
        try:
            page = Page.objects.get(name=resource)
            content = page.content
            response = "Contenido: " + content + "<br>" + form.format(value=content)
        except Page.DoesNotExist:
            response = form.format(value="")

        return HttpResponse(response)

    elif request.method == "POST":
        content = request.POST["rec"]
        try:
            page = Page.objects.get(name=resource)
            page.content = content
            page.save()
        except Page.DoesNotExist:
            page = Page(name=resource, content=content)
            page.save()
        return HttpResponse("Realiza un GET a / para comprobar que se ha añadido correctamente.")
