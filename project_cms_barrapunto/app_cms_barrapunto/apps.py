from django.apps import AppConfig


class AppCmsBarrapuntoConfig(AppConfig):
    name = 'app_cms_barrapunto'
